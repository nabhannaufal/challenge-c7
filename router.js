const router = require("express").Router();
const auth = require("./controllers/authController");
const nav = require("./controllers/navigationController");
const sup = require("./controllers/superUserController");
const player = require("./controllers/userPlayerController");
const restrictApi = require("./middlewares/restrictApi");
const restrict = require("./middlewares/restrict");

//Routes API
router.post("/api/v1/auth/register", auth.registerApi);
router.post("/api/v1/auth/login", auth.loginApi);
router.get("/api/v1/auth/whoami", restrictApi, auth.whoamiApi);

//Routes Auth
router.get("/register", auth.registerView);
router.get("/login", auth.loginView);
router.post("/login", auth.login);
router.post("/register", auth.register);
router.get("/logout", auth.logout);

//Routes Navigaton
router.get("/", nav.index);

//Routes Super User
router.get("/dashboard", restrict, sup.dashboard);
router.get("/history", restrict, sup.history);
router.get("/superuser/add", restrict, sup.addSuperUserView);
router.post("/superuser/add", restrict, sup.addSuperUser);
router.get("/superuser/delete/:id", restrict, sup.deleteSuperUser);
router.get("/superuser/update/:id", restrict, sup.updateSuperUserView);
router.post("/superuser/update/:id", restrict, sup.updateSuperUser);

//Routes User Player
router.get("/userplayer/add", restrict, player.addUserView);
router.post("/userplayer/add", restrict, player.addUser);
router.get("/userplayer", restrict, player.getAllUser);
router.get("/userplayer/delete/:id", restrict, player.deleteUserPlayer);
router.get("/userplayer/update/:id", restrict, player.updateUserPlayerView);
router.post("/userplayer/update/:id", restrict, player.updateUserPlayer);

module.exports = router;
