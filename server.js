const express = require("express");
const app = express();
const { PORT = 8000 } = process.env;
const session = require("express-session");
const flash = require("express-flash");
const passportJwt = require("./lib/passportJwt");
const passportLocal = require("./lib/passportLocal");
const router = require("./router");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  session({
    secret: "nabhan secret",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passportJwt.initialize());
app.use(passportLocal.initialize());
app.use(passportLocal.session());
app.use(flash());
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(router);

app.listen(PORT, () => {
  console.log(`Server nyala di port ${PORT}`);
});
