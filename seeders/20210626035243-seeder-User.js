"use strict";
const bcrypt = require("bcrypt");
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          username: "admin",
          password: bcrypt.hashSync("admin", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "nabhan",
          password: bcrypt.hashSync("nabhan", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
