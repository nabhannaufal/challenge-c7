"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "user_game_biodata",
      [
        {
          namaLengkap: "Nabhan Naufal",
          email: "nabhan@gmail.com",
          alamat: "Bogor Baru",
          noTelpon: "+62818666040",
          urlFoto: "https://www.w3schools.com/howto/img_avatar.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          namaLengkap: "Naufal Muhammad",
          email: "naufal@gmail.com",
          alamat: "Arzimar III",
          noTelpon: "+62818777040",
          urlFoto: "https://www.w3schools.com/w3images/avatar2.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          namaLengkap: "Siti Devi",
          email: "devi@gmail.com",
          alamat: "Los Angeles",
          noTelpon: "+62818999040",
          urlFoto: "https://www.w3schools.com/howto/img_avatar2.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          namaLengkap: "Siti Afika",
          email: "fika@gmail.com",
          alamat: "New York",
          noTelpon: "+62818555040",
          urlFoto: "https://www.w3schools.com/howto/img_avatar2.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          namaLengkap: "Zana Phoebe",
          email: "phoebe@gmail.com",
          alamat: "Yogyakarta",
          noTelpon: "+62818111040",
          urlFoto: "https://www.w3schools.com/howto/img_avatar2.png",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("user_games_biodata", null, {});
  },
};
