"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "user_game_histories",
      [
        {
          idBattlle: "8xpSOC3DgZpTCit",
          skor: "200",
          namaPlayer: "nabhan",
          resultGame: "Win",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "KeoBk6OZCMBmjR6",
          skor: "100",
          namaPlayer: "nabhan",
          resultGame: "Draw",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "bh073FTKp9G0Y05",
          skor: "-100",
          namaPlayer: "naufal",
          resultGame: "Lose",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "nyjIwe73FsOO06f",
          skor: "200",
          namaPlayer: "devi",
          resultGame: "Win",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "XpeADj9coZUc4M5",
          skor: "-100",
          namaPlayer: "nabhan",
          resultGame: "Lose",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "stWsO6fuS61PsLt",
          skor: "200",
          namaPlayer: "fika",
          resultGame: "Win",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "t3ovP8oULHho5L2",
          skor: "200",
          namaPlayer: "phoebe",
          resultGame: "Win",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "gSnzeWftuZjjPol",
          skor: "100",
          namaPlayer: "naufal",
          resultGame: "Draw",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "gSnzeWftuZjjPol",
          skor: "-100",
          namaPlayer: "naufal",
          resultGame: "Lose",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          idBattlle: "iV8VxfI6mj7nfmj",
          skor: "200",
          namaPlayer: "phoebe",
          resultGame: "Win",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("user_games_histories", null, {});
  },
};
