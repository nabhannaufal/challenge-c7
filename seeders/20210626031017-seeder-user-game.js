"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "user_games",
      [
        {
          username: "nabhan",
          password: "nabhan",
          cash: 100000,
          level: "Expert",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "naufal",
          password: "naufal",
          cash: 200000,
          level: "Intermediate",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "devi",
          password: "devi",
          level: "Beginner",
          cash: 100000,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "fika",
          password: "fika",
          cash: 200000,
          level: "Beginner",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "phoebe",
          password: "phoebe",
          cash: 900000,
          level: "Pro",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("user_games", null, {});
  },
};
