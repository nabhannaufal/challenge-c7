const { User } = require("../models");
const passport = require("../lib/passportLocal");
function format(user) {
  const { id, username } = user;
  return {
    id,
    username,
    accessToken: user.generateToken(),
  };
}

module.exports = {
  registerApi: (req, res, next) => {
    User.register(req.body)
      .then(() => {
        res.json({ message: "anda berhasil daftar" });
      })
      .catch((err) => next(err));
  },
  loginApi: (req, res) => {
    User.authenticate(req.body).then((user) => {
      res.json(format(user));
    });
  },
  whoamiApi: (req, res) => {
    const currentUser = req.user;
    res.json(currentUser);
  },
  register: (req, res, next) => {
    User.register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },
  login: passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/login",
    failureFlash: true,
  }),

  loginView: (req, res) => {
    res.render("login");
  },

  registerView: (req, res) => {
    res.render("register");
  },

  logout: (req, res) => {
    req.logout();
    res.redirect("/login");
  },
};
