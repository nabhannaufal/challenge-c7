module.exports = {
  index: (req, res) => {
    res.redirect("/register");
  },

  invalidUrl: (req, res) => {
    res.redirect("/");
  },
};
