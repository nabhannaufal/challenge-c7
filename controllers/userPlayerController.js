const { User, user_game, user_game_biodata } = require("../models");

module.exports = {
  addUserView: (req, res) => {
    const namaUser = req.user.dataValues.username;
    res.render("addUser", { userNama: namaUser });
  },
  addUser: (req, res) => {
    user_game
      .create({
        username: req.body.username,
        password: req.body.password,
        cash: req.body.cash,
        level: req.body.level,
      })
      .then((userGame) => {
        user_game_biodata
          .create({
            namaLengkap: req.body.namaLengkap,
            email: req.body.email,
            alamat: req.body.alamat,
            noTelpon: req.body.noTelpon,
            urlFoto: req.body.urlFoto,
          })
          .then((biodata) => res.redirect("/dashboard"));
      });
  },
  getAllUser: (req, res) => {
    const namaUser = req.user.dataValues.username;
    user_game
      .findAll()
      .then((userGame) =>
        User.findAll().then((superUser) =>
          res.render("user_view", { userGame, superUser, userNama: namaUser })
        )
      );
  },
  deleteUserPlayer: (req, res) => {
    const userId = req.params.id;
    user_game_biodata
      .destroy({
        where: {
          id: userId,
        },
      })
      .then((biodata) => {
        user_game
          .destroy({
            where: {
              id: userId,
            },
          })
          .then((user) => {
            res.redirect("/dashboard");
          });
      });
  },
  updateUserPlayerView: (req, res) => {
    const userId = req.params.id;
    const namaUser = req.user.dataValues.username;
    user_game
      .findOne({
        where: {
          id: userId,
        },
      })
      .then((user) => {
        user_game_biodata
          .findOne({
            where: {
              id: user.id,
            },
          })
          .then((biodata) => {
            res.render("updateUserView", {
              user,
              biodata,
              userNama: namaUser,
            });
          });
      });
  },
  updateUserPlayer: (req, res) => {
    const userId = req.params.id;
    user_game
      .update(
        {
          username: req.body.username,
          password: req.body.password,
          cash: req.body.cash,
          level: req.body.level,
        },
        {
          where: {
            id: userId,
          },
        }
      )
      .then((user) => {
        user_game_biodata
          .update(
            {
              namaLengkap: req.body.namaLengkap,
              email: req.body.email,
              alamat: req.body.alamat,
              noTelpon: req.body.noTelpon,
              urlFoto: req.body.urlFoto,
            },
            {
              where: {
                id: userId,
              },
            }
          )
          .then((biodata) => {
            res.redirect("/dashboard");
          });
      });
  },
};
