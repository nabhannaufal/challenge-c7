const {
  User,
  user_game,
  user_game_biodata,
  user_game_history,
} = require("../models");

module.exports = {
  dashboard: (req, res) => {
    const namaUser = req.user.dataValues.username;
    user_game.findAll().then((userGame) =>
      user_game_biodata.findAll().then((userBio) =>
        user_game_history.findAll().then((userHistory) =>
          User.findAll().then((superUser) =>
            res.render("dashboard", {
              userGame,
              userBio,
              userHistory,
              superUser,
              userNama: namaUser,
            })
          )
        )
      )
    );
  },
  history: (req, res) => {
    const namaUser = req.user.dataValues.username;
    user_game_history
      .findAll()
      .then((userHistory) =>
        res.render("history", { userHistory, userNama: namaUser })
      );
  },
  addSuperUserView: (req, res) => {
    const namaUser = req.user.dataValues.username;
    res.render("addSuperUser", { userNama: namaUser });
  },
  addSuperUser: (req, res) => {
    User.register(req.body)
      .then(() => {
        res.redirect("/dashboard");
      })
      .catch((err) => next(err));
  },
  deleteSuperUser: (req, res) => {
    const adminId = req.params.id;
    User.destroy({
      where: {
        id: adminId,
      },
    }).then((admin) => res.redirect("/dashboard"));
  },
  updateSuperUserView: (req, res) => {
    const namaUser = req.user.dataValues.username;
    const superUserId = req.params.id;
    User.findOne({
      where: {
        id: superUserId,
      },
    }).then((superUser) =>
      res.render("updateSuperUser", { superUser, userNama: namaUser })
    );
  },
  updateSuperUser: (req, res, next) => {
    const superUserId = req.params.id;
    const kondisi = {
      where: {
        id: superUserId,
      },
    };

    User.updateAkun(req.body, kondisi)
      .then(() => {
        res.redirect("/dashboard");
      })
      .catch((err) => next(err));
  },
};
